import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main(){
  runApp(const Alert());
}
class Alert extends StatelessWidget {
  const Alert({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
        debugShowCheckedModeBanner: false,
        home:HomePage()/*class name kuch bhi le skte hai*/
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ElevatedButton(onPressed: (){
          showDialog(
            context: context,
            builder: (context) => AlertDialog(

              title: Row(
                children: const [
                  Icon(Icons.delete),
                  Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Text("ALert Dialog!"),
                  ),
                ],
              ),
              content: const Text("Are you sure you want to delete"),
              actions: [
                TextButton(onPressed: (){
                  Navigator.of(context).pop();
                  Fluttertoast.showToast(msg: "Deleted Successful");
                },
                  child:const Text('Yes',style: TextStyle(color: Colors.black45),) ,
                ),
                TextButton(onPressed: (){
                  Navigator.of(context).pop();
                  Fluttertoast.showToast(msg: 'ok');
                },
                  child: const Text('Cancel',style: TextStyle(color: Colors.black45),) ,)
              ],
            ),);
        },
            child:const Text("Click Me") ),
      ),
    );
  }
}