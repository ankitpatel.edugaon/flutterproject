import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool passwordVisible=false;

  @override
  void initState(){
    super.initState();
    passwordVisible=true;
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(Icons.logout),
          )
        ],
        title: Center(
          child: Text("Login Page"),
        ),
      ),
      drawer: Drawer(),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 60),
            child: Center(
              child: Text(
                "SignIn",
                style: TextStyle(color: Colors.blue, fontSize: 35),
              ),
            ),
          ),

          SizedBox(height: 40),

          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30),
            child: TextField(
              decoration: InputDecoration(
                  label: Text('UserName'),
                  border: OutlineInputBorder(),
                  hintText: "UserName"),
            ),
          ),

          SizedBox(height: 20),

          Padding(
            padding: EdgeInsets.only(left: 30, right: 30),
            child: TextField(
              obscureText: passwordVisible,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Password",
                labelText: "Password",
                helperText:"Password must contain special character",
                helperStyle:TextStyle(color:Colors.green),
                suffixIcon: IconButton(
                  icon: Icon(passwordVisible
                      ? Icons.visibility
                      : Icons.visibility_off),
                  onPressed: () {
                    setState(
                          () {
                        passwordVisible = !passwordVisible;
                      },
                    );
                  },
                ),
                alignLabelWithHint: false,
                filled: true,
              ),
              keyboardType: TextInputType.visiblePassword,
              textInputAction: TextInputAction.done,
            ),
          ),

          TextButton(
            onPressed: () {
              //forgot password screen
            },
            child: const Padding(
              padding: EdgeInsets.only(left: 200),
              child: Text('Forgot Password'),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(left: 30, right: 30),
            child: Container(
              width: 830,
              height: 40,
              child: ElevatedButton(
                onPressed: () {
                  Fluttertoast.showToast(msg:'Login Successful');
                },
                child: Text("SignIn"),
              ),
            ),
          ),

          SizedBox(height: 20),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text('Does Not Have Account?', style: TextStyle(color: Colors.black),),

              Text('SignIn', style: TextStyle(color: Colors.blue,fontWeight: FontWeight.w900,fontSize: 15),)
            ],
          )
        ],
      ),
    );
  }
}
