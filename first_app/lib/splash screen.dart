import 'package:first_app/LoginPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: PreferredSize(
      //     child: AppBar(
      //         backgroundColor: CupertinoColors.activeBlue,
      //         title: Text("Splash")),
      //     preferredSize: Size.fromHeight(100)),
      body: Column(
        children: [
          Container(
            height: 550,
            width: 500,
            child: Image.asset("asset/images/splashimg.jpg"),
          ),
          ElevatedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LoginPage()));
              },
              child: Text("Next-Page"))
        ],
      ),
    );
  }
}
