import 'package:flutter/material.dart';

class ListViewBuilder extends StatelessWidget {
  final List<String> items = [
    '1:- Ankit Patel',
    '2:- Suraj Singh',
    '3:- Amit Kumar',
    '4:- Anurag Kumar',
    '5:- Sahil Kumar',
    '6:- Abhishek Kumar',
    '7:- Arun Kumar',
    '8:- Golatech',
    '9:- Sonu Kumar',
    '10:- Nitesh Raj Patel',
    '11:- Nitish Kumar',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:Center(child: Text('Flutter ListView Builder')),
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(items[index]),
          );
        },
      ),
    );
  }
}
