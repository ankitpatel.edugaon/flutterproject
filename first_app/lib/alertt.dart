import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


class alert extends StatefulWidget {
  const alert({Key? key}) : super(key: key);

  @override
  State<alert> createState() => _alertState();
}

class _alertState extends State<alert> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child:Center(
          child: ElevatedButton(onPressed: (){
            showDialog(
              context: context,
              builder: (context) => AlertDialog(
                title: Row(
                  children: [
                    Icon(Icons.delete),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text("ALert Dialog!"),
                    ),
                  ],
                ),
                content: Text("Are you sure you want to delete"),
                actions: [
                  TextButton(onPressed: (){
                    Navigator.of(context).pop();
                    Fluttertoast.showToast(msg: "Deleted Successful");
                  },
                    child:Container(
                      child: Text('Yes',style: TextStyle(color: Colors.black45),),
                    ) ,
                  ),
                  TextButton(onPressed: (){
                    Navigator.of(context).pop();
                    Fluttertoast.showToast(msg:"Cancel");
                  },
                    child: Container(
                      child: Text('Cancel',style: TextStyle(color: Colors.black45),),) ,)
                ],
              ),);
          },
              child:Text("Click Me") ),
        ),
      ),
    );
  }
}

