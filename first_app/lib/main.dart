import 'package:first_app/alertt.dart';
import 'package:first_app/splash%20screen.dart';
import 'package:flutter/material.dart';
import 'ListView.dart';
import 'ListViewBuilder.dart';
import 'LoginPage.dart';
import 'buttonnavigationbar.dart';


void main(){
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyStatefulWidget(),
    );
  }
}

