import 'package:flutter/material.dart';

class ListViewBuilder extends StatefulWidget {
  const ListViewBuilder({Key? key}) : super(key: key);

  @override
  State<ListViewBuilder> createState() => _ListViewBuilderState();
}

class _ListViewBuilderState extends State<ListViewBuilder> {

  @override
  Widget build(BuildContext context) {
    var list = ['1:- Ankit Patel',
      '2:- Suraj Singh',
      '3:- Amit Kumar',
      '4:- Anurag Kumar',
      '5:- Sahil Kumar',
      '6:- Abhishek Kumar',
      '7:- Arun Kumar',
      '8:- Golatech',
      '9:- Sonu Kumar',
      '10:- Nitesh Raj Patel',
      '11:- Nitish Kumar',];
    return Scaffold(
        appBar: AppBar(
          title: const Center(child: Text('Flutter ListView Builder')),
        ),
        body: ListView.builder(
            itemBuilder: (context, index)  {
            return ListTile(
               title: Text(list[index]),
           );
    },
            itemCount: list.length
    ),);
  }
}
