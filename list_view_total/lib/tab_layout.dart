import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TabLayout extends StatefulWidget {
  const TabLayout({Key? key}) : super(key: key);

  @override
  State<TabLayout> createState() => _TabLayoutState();
}

class _TabLayoutState extends State<TabLayout> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            actions: [
              PopupMenuButton(
                itemBuilder: (context) =>[
                  const PopupMenuItem(child: Text('New group')),
                  const PopupMenuItem(child: Text('New broadcast')),
                  const PopupMenuItem(child: Text('Linked devices')),
                  const PopupMenuItem(child: Text('Started messages')),
                  const PopupMenuItem(child: Text('Payments')),
                  const PopupMenuItem(child: Text('Settings')),
                ],
              )
            ],
            bottom: const TabBar(
              tabs: [
                Tab(
                  icon: Icon(Icons.chat),
                  child: Text('Chat'),
                ),
                Tab(icon: Icon(Icons.track_changes), child: Text('Status')),
                Tab(icon: Icon(Icons.call), child: Text('Call')),
              ],
            ),
            title: const Text('Tab Layout'),
          ),
          body: const TabBarView(
            children: [
              Icon(Icons.chat),
              Icon(Icons.track_changes),
              Icon(Icons.call),
            ],
          ),
        ),
      ),
    );
  }
}
