import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:list_view_total/pages/snackbar.dart';

class AllWidgets extends StatefulWidget {
  const AllWidgets({Key? key}) : super(key: key);

  @override
  State<AllWidgets> createState() => _AllWidgetsState();
}

class _AllWidgetsState extends State<AllWidgets> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('All Widgets')),
      body: ListView(
        children: [
          ButtonBar(
            alignment: MainAxisAlignment.center,
            children: [

              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const SnackBarpage(),
                          ));
                    },
                    child: const Text('SnackBar'),
                  ),
                ),
              ),
              const SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SliverGrid");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => Slivergrid(),
                          ));
                    },
                    child: Text('SliverGrid'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SliverAppBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const SliverAppBarScreen(),
                          ));
                    },
                    child: const Text('SliverAppBar'),
                  ),
                ),
              ),
              const SizedBox(height: 10),

            
              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SliverList");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const SliverListPage(),
                          ));
                    },
                    child: const Text('SliverList'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "AnimatedIcon");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AnimatedIconPage(),
                          ));
                    },
                    child: Text('AnimatedIcon'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "Expanded");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ExpandedPage(),
                          ));
                    },
                    child: Text('ExpandedPage'),
                  ),
                ),
              ),
              const SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "StackWidgets");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => const StackWidgets(),
                          ));
                    },
                    child:const Text('StackWidgets'),
                  ),
                ),
              ),
              const SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SnackBarpage(),
                          ));
                    },
                    child: Text('SnackBar'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SnackBarpage(),
                          ));
                    },
                    child: Text('SnackBar'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SnackBarpage(),
                          ));
                    },
                    child: Text('SnackBar'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SnackBarpage(),
                          ));
                    },
                    child: Text('SnackBar'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SnackBarpage(),
                          ));
                    },
                    child: Text('SnackBar'),
                  ),
                ),
              ),
              SizedBox(height: 10),


              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SnackBarpage(),
                          ));
                    },
                    child: Text('SnackBar'),
                  ),
                ),
              ),
              SizedBox(height: 10),

              
              SizedBox(
                height: 50,
                width: 200,
                child: Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: ElevatedButton(
                    onPressed: () {
                      Fluttertoast.showToast(msg: "SnackBar");
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SnackBarpage(),
                          ));
                    },
                    child: Text('SnackBar'),
                  ),
                ),
              ),
              SizedBox(height: 10),
            ],
          ),
        ],
      ),
    );
  }
}
