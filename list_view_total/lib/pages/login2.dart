import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  const LoginPage ({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool passwordVisible=false;

  @override
  void initState(){
    super.initState();
    passwordVisible=true;
  }
  @override
  Widget build(BuildContext context) {
    return  MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        // appBar: AppBar(
        //   title: Text(
        //     "Simple Practise Ui",
        //     style: TextStyle(
        //         color: Colors.white,
        //         backgroundColor: Colors.blue,
        //         fontWeight: FontWeight.bold),
        //   ),
        //   leading: Icon(Icons.menu),
        //   actions: [
        //     IconButton(onPressed: () {
        //     }, icon: Icon(Icons.logout)),
        //   ],
        // ),
        // drawer: Drawer(),
        body: ListView(
          children: [
            Column(
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 120),
                  child:  CircleAvatar(
                    radius: 50,
                    backgroundImage: AssetImage("assests/images/edugaon.png"),
                  ),
                ),

                const SizedBox(height: 25),
                const Padding(
                  padding: EdgeInsets.only(top: 0),
                  child: Center(
                    child: Text(
                      "Login",
                      style: TextStyle(color: Colors.blue, fontSize: 35),
                    ),
                  ),
                ),
                const SizedBox(height: 30),
                const Padding(
                  padding: EdgeInsets.only(left: 30, right: 30),
                  child: TextField(
                    decoration: InputDecoration(
                        prefixIcon: Icon(Icons.person),
                        label: Text('UserName'),
                        border: OutlineInputBorder(),
                        hintText: "UserName"),
                    keyboardType: TextInputType.emailAddress,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only( top:30,left: 30, right: 30),
                  child: TextField(
                    obscureText: passwordVisible,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: "Password",
                      labelText: "Password",
                      helperText:"Password must contain special character",
                      helperStyle:TextStyle(color:Colors.green),
                      suffixIcon: IconButton(
                        icon: Icon(passwordVisible
                            ? Icons.visibility
                            : Icons.visibility_off),
                        onPressed: () {
                          setState(
                                () {
                              passwordVisible = !passwordVisible;
                            },
                          );
                        },
                      ),
                      alignLabelWithHint: false,
                      filled: true,
                    ),
                    keyboardType: TextInputType.visiblePassword,
                    textInputAction: TextInputAction.done,
                  ),
                ),

                Row(
                  children: [
                    InkWell(
                      onTap: (){},
                      child: const Padding(
                        padding: EdgeInsets.only(top: 10,left:230),
                        child: Text(
                          'Forget Password?',
                          style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.w900,
                              fontSize: 15),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30, top: 20, right: 30),
                  child: SizedBox(
                    width: 500,
                    height: 50,
                    child: ElevatedButton(onPressed: () {
                      Fluttertoast.showToast(msg: 'Login Successful',
                          fontSize: 12,
                          textColor: Colors.white,
                          toastLength: Toast.LENGTH_LONG,
                          backgroundColor: Colors.cyan);
                    }, child: const Text("Login")),
                  ),
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text(
                      'Does Not Have Account?',
                      style: TextStyle(color: Colors.black,fontSize: 15),
                    ),
                    InkWell(
                      onTap: (){},
                      child: const Text(
                        'SignUp',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w900,
                            fontSize: 15),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Padding(
                  padding: const EdgeInsets.all(0),
                  child: Wrap(
                    alignment: WrapAlignment.center,
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: SizedBox(width: 50, height: 50,
                          child: CircleAvatar(
                            backgroundImage: AssetImage("assests/images/google.png"),
                          ),
                        ),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(right: 10),
                        child: SizedBox(width: 50, height: 50,
                          child: CircleAvatar(
                            backgroundImage: AssetImage("assests/images/facebook.png"),
                          ),
                        ),
                      ),
                      InkWell(
                        onTap: (){
                        },
                        child: const Padding(
                          padding: EdgeInsets.only(left: 20),
                          child: SizedBox(width: 50, height: 50,
                            child: CircleAvatar(
                              backgroundImage: AssetImage("assests/images/phoneauthentication.png"),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],

            ),
          ],
        ),
      ),
    );
  }
}

