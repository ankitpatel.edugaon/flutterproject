import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WhatsApp2 extends StatefulWidget {
  const WhatsApp2({Key? key}) : super(key: key);

  @override
  State<WhatsApp2> createState() => _WhatsApp2State();
}

class _WhatsApp2State extends State<WhatsApp2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          ListTile(
            title: Text('Ankit Patel'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 am'),
            textColor: Colors.blue,
            tileColor:CupertinoColors.systemYellow,
            onTap: () {},
          ),
          ListTile(
            title: Text('Nitesh Raj Patel'),
            subtitle: Text('aaj padhne jana hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('08:05 pm'),
            textColor: Colors.blue,
            tileColor: Colors.red,
            onTap: () {},
          ),
          ListTile(
            title: Text('Nitish Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image.asset("assests/images/nitish.jpg"),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Sahil Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Abhishek Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Amit Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Anurag Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Golden Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Sonu Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Arun Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Sahil Kumar'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Sudish Bhaiya'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
          ListTile(
            title: Text('Rahul Sir'),
            subtitle: Text('Kya ho rha hai'),
            leading: Image(image: AssetImage("assests/images/ankit.jpg")),
            trailing: Text('12:00 AM'),
            textColor: Colors.blue,
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
