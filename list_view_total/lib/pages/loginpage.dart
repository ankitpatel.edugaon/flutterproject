import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';


class Login1 extends StatefulWidget {
  const Login1({Key? key}) : super(key: key);

  @override
  State<Login1> createState() => _Login1State();
}

class _Login1State extends State<Login1> {
  bool passwordVisible = false;

  @override
  void initState() {
    super.initState();
    passwordVisible = true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("assests/images/loginbg.png"),
              fit: BoxFit.cover)),
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            Container(
              padding: EdgeInsets.only(left: 35, top: 170),
              child: Text(
                "Welcome \n Back",
                style: TextStyle(fontSize: 28, color: Colors.white),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * 0.45,
            right: 35,
            left: 35,
              ),
              child: Column(
            children: [
              TextField(
                decoration: InputDecoration(
                    hintText: "UserName",
                    fillColor: Colors.grey.shade100,
                    prefixIcon: Icon(Icons.person),
                    label: Text('UserName'),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10))),
              ),
              SizedBox(height: 25),
              TextField(
                obscureText: passwordVisible,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10)),
                  hintText: "Password",
                  labelText: "Password",
                  helperText: "Password must contain special character",
                  helperStyle: TextStyle(color: Colors.green),
                  fillColor: Colors.grey.shade100,
                  suffixIcon: IconButton(
                    icon: Icon(passwordVisible
                        ? Icons.visibility
                        : Icons.visibility_off),
                    onPressed: () {
                      setState(
                        () {
                          passwordVisible = !passwordVisible;
                        },
                      );
                    },
                  ),
                  alignLabelWithHint: false,
                  filled: true,
                ),
                keyboardType: TextInputType.visiblePassword,
                textInputAction: TextInputAction.done,
              ),
              Row(
                children: [
                  // InkWell(
                  //   onTap: (){
                  //     Navigator.push(context, MaterialPageRoute(builder: (context)=>ForgetPasswordPage()));
                  //   },
                  //   child: Padding(
                  //     padding: const EdgeInsets.only(top: 10,left:190),
                  //     child: Text(
                  //       'Forget Password?',
                  //       style: TextStyle(
                  //           color: Colors.blue,
                  //           fontWeight: FontWeight.w900,
                  //           fontSize: 15),
                  //     ),
                  //   ),
                  // ),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(left: 10, top: 20, right: 10),
                child: Container(
                  width: 500,
                  height: 50,
                  child: ElevatedButton(onPressed: () {
                    Fluttertoast.showToast(msg: 'Login Successful',
                        fontSize: 12,
                        textColor: Colors.white,
                        toastLength: Toast.LENGTH_LONG,
                        backgroundColor: Colors.cyan);
                  }, child: Text("Login")),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Does Not Have Account?',
                    style: TextStyle(color: Colors.black,fontSize: 15),
                  ),
                  // InkWell(
                  //   onTap: (){
                  //     Navigator.push(context, MaterialPageRoute(builder: (context)=>RegisterPage()));
                  //   },
                  //   child: Text(
                  //     'SignUp',
                  //     style: TextStyle(
                  //         color: Colors.blue,
                  //         fontWeight: FontWeight.w900,
                  //         fontSize: 15),
                  //   ),
                  // ),
                ],
              ),
              SizedBox(height: 20),
              Padding(
                padding: EdgeInsets.all(0),
                child: Wrap(
                  alignment: WrapAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.only(right: 20),
                      child: Container(width: 50, height: 50,
                        child: CircleAvatar(
                          backgroundImage: AssetImage("assests/images/google.png"),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 10),
                      child: Container(width: 50, height: 50,
                        child: CircleAvatar(
                          backgroundImage: AssetImage("assests/images/facebook.png"),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 20),
                      child: Container(width: 50, height: 50,
                        child: CircleAvatar(
                          backgroundImage: AssetImage("assests/images/phoneauthentication.png"),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
