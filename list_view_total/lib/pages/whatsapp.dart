import 'package:flutter/material.dart';


class WhatsApp extends StatefulWidget {
  const WhatsApp({Key? key}) : super(key: key);

  @override
  State<WhatsApp> createState() => _WhatsAppState();
}

class _WhatsAppState extends State<WhatsApp> {
  @override
  Widget build(BuildContext context) {
    var name = [
      'Ankit Patel',
      'Suraj Singh',
      'Amit Kumar',
      'Anurag Kumar',
      'Sahil Kumar',
      'Abhishek Kumar',
      'Arun Kumar',
      'Golatech',
      'Sonu Kumar',
      'Nitesh Raj Patel',
      'Nitish Kumar',
      'Arjun Kumar',
      'Wajid Ali',
      'Ajay Kumar',
      'Maasum Kumar',
    ];
    var image=[
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
      "assests/images/ankit.jpg",
    ];
    var decereptions=[
      "kya haal hai",
      "kaise ho bhai",
      "aaj class jana hai ki nahi",
      "flutter sir aaj padhaye ki nahi",
      "class me kya padhai hua hai",
      "aao market aaj party dena",
      "Sudish sir aaj padhaye hai",
      "Register wala Code bhejo",
      "Splash ka code email karo",
      "kya sunn rahe hai aaj kal",
      "aaj mai class nahi aaunga ",
      "list view kaise banaye",
      "circle image kaise lagega",
      "android me gradle problem",
      "app ka icon kaise lagaye",
    ];
    var lastseen=[
      "9:30 am",
      "7:14 pm",
      "just now",
      "11:01 am",
      "9:30 pm",
      "9:30 am",
      "9:30 pm",
      "9:30",
      "9:30",
      "9:30",
      "9:30",
      "9:30",
      "9:30",
      "9:30",
      "9:30",
    ];
    return MaterialApp(
      title: "Whatsapp",
      home: Scaffold(
          appBar: AppBar(
            title: Text("Whatsapp"),
          ),
          body: ListView.separated(
            itemCount: 15,
            itemBuilder: (BuildContext context, index) {
              return Container(
                  height: 100,
                  color: Colors.grey,
                  child: Wrap(
                    children: [
                      const Padding(
                        padding:
                            EdgeInsets.only(left: 5, top: 5, bottom: 5),
                        child: CircleAvatar(
                          radius: 40,
                          backgroundImage: AssetImage(
                              "assests/images/ankit.jpg"),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 0),
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(top: 5),
                              child: Text("${name[index]}",
                                  style: const TextStyle(
                                      color: Colors.pink,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                            ),
                             Padding(
                              padding: EdgeInsets.only(left: 15),
                              child: Text("${decereptions[index]}",
                                  style: const TextStyle(
                                      color: Colors.pink,
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold)),
                            ),
                          ],
                        ),
                      ),
                       Padding(
                        padding: EdgeInsets.only(left: 40,top: 20),
                        child: Text("${lastseen[index]}"),
                      )
                    ],
                  ));
            },
            separatorBuilder: (BuildContext context, index) => const Divider(),

          ),

      ),

    );
  }
}
