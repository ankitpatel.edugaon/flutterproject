import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../ListViewButton.dart';


class Splash extends StatefulWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  State<Splash> createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  @override
  void initState() {
    super.initState();
    Timer(
        const Duration(milliseconds: 1000),
            () => Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => const ListViewButton())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Center(
            child: Text("Splash",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold)),
          ),
          Center(child: CircularProgressIndicator())
        ],
      ),
    );
  }
}
