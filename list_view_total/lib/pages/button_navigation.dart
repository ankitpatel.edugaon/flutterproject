import 'package:flutter/material.dart';
import 'package:list_view_total/pages/whatsapp.dart';

import 'home.dart';

class ButtonNavigation extends StatefulWidget {
  const ButtonNavigation({Key? key}) : super(key: key);

  @override
  State<ButtonNavigation> createState() => _ButtonNavigationState();
}

class _ButtonNavigationState extends State<ButtonNavigation> {
  int  selectedPageIndex =0;
  var screens = [
    const MaterialApp(
      home: WhatsApp(
      ),
    ),
    MaterialApp(color: Colors.orange,
      home:Scaffold( appBar: AppBar(title: const Text("Notification"),)),
    ),
    MaterialApp(
      home: ProfilePage(
      ),
    ),
  ];
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: "Home",
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: screens[selectedPageIndex],
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: selectedPageIndex,
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
              BottomNavigationBarItem(icon: Icon(Icons.notification_add), label: "Notification"),
              BottomNavigationBarItem(icon: Icon(Icons.account_circle,size: 25,), label: "Profile")
            ],
            onTap: (index){
              selectedPageIndex = index;
              setState(() {

              });
            },
          ),
        )
    );
  }
}