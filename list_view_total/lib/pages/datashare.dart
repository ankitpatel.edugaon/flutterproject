// ignore: implementation_imports
import 'package:flutter/material.dart';


class ShareData extends StatefulWidget {
  const ShareData({super.key});

  @override
  State<ShareData> createState() => _ShareDataState();
}
TextEditingController name=TextEditingController();
TextEditingController email=TextEditingController();

class _ShareDataState extends State<ShareData> {
  @override
  Widget build(BuildContext context) {
    return Container(
    
      child: Column(
        children: [
          TextField(
            controller: name,
            decoration: InputDecoration(
              border: OutlineInputBorder(borderRadius: BorderRadius.all(30 as Radius))
            ),),
          TextField(controller: email,
          decoration: InputDecoration(border: OutlineInputBorder(borderRadius: BorderRadius.all(30 as Radius))),),
          ]),
    );
  }
}