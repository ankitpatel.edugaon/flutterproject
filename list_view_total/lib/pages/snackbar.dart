import 'dart:math';

import 'package:flutter/material.dart';

//SnackBarPage with Stateful widgets//

class SnackBarpage extends StatefulWidget {
  const SnackBarpage({super.key});
  @override
  State<SnackBarpage> createState() => _SnackBarpageState();
}

class _SnackBarpageState extends State<SnackBarpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("SnackBar"),
      ),
      body: Center(
          child: ElevatedButton(
              onPressed: () {
                final message = SnackBar(
                  content: const Text('This is a SnackBar'),
                  backgroundColor: Colors.yellow,
                  elevation: 10,
                  behavior: SnackBarBehavior.floating,
                  margin: const EdgeInsets.all(10),
                  action: SnackBarAction(label: "undo", onPressed: () {}),
                );
                ScaffoldMessenger.of(context).showSnackBar(message);
              },
              child: const Text("SnackBar"))),
    );
  }
}

//SliverAppBarPage with Stateful widgets//

class SliverAppBarScreen extends StatefulWidget {
  const SliverAppBarScreen({super.key});

  @override
  State<SliverAppBarScreen> createState() => _SliverAppBarScreenState();
}

class _SliverAppBarScreenState extends State<SliverAppBarScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange,
      appBar: AppBar(title: const Text('Sliver App Bar Demo')),
      body: CustomScrollView(
        slivers: <Widget>[
          const SliverAppBar(
            pinned: false,
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
              title: Text('SliverAppBar'),
            ),
          ),
          SliverFixedExtentList(
            itemExtent: 50.0,
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Container(
                  alignment: Alignment.center,
                  color: Colors.lightBlue[100 * (index % 9)],
                  child: const Text('List Item index'), // Add doller for index
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}

//SliverGridPage with Stateful widgets//

class Slivergrid extends StatefulWidget {
  const Slivergrid({super.key});
  @override
  State<Slivergrid> createState() => _SlivergridState();
}

class _SlivergridState extends State<Slivergrid> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Sliver Grid')),
        body: CustomScrollView(
          slivers: <Widget>[
            SliverGrid(
                gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                  maxCrossAxisExtent: 50,
                ),
                delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) {
                  return Container(
                      color:
                          Color((Random().nextDouble() * 0xFFFFFF).toInt() << 0)
                              .withOpacity(1.0),
                      height: 100.0);
                }))
          ],
        ));
  }
}

//SliverListPage with Stateful widgets//

class SliverListPage extends StatefulWidget {
  const SliverListPage({super.key});

  @override
  State<SliverListPage> createState() => _SliverListPageState();
}

class _SliverListPageState extends State<SliverListPage> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverList(
          delegate: SliverChildBuilderDelegate(
            (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: index % 2 == 0 ? Colors.green : Colors.greenAccent,
                  height: 80,
                  alignment: Alignment.center,
                  child: Text(
                    "Item $index",
                    style: const TextStyle(fontSize: 30),
                  ),
                ),
              );
            },
            // 40 list items
            childCount: 40,
          ),
        ),
      ],
    );
  }
}

//SliverGridViewPage with Stateful widgets//

class AnimatedIconPage extends StatefulWidget {
  const AnimatedIconPage({super.key});
  @override
  State<AnimatedIconPage> createState() => _AnimatedIconPageState();
}

class _AnimatedIconPageState extends State<AnimatedIconPage>
    with SingleTickerProviderStateMixin {
  late AnimationController controller;
  late Animation<double> animation;

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 2),
    )
      ..forward()
      ..repeat(reverse: true);
    animation = Tween<double>(begin: 0.0, end: 1.0).animate(controller);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: animation,
          size: 72.0,
          semanticLabel: 'Show menu',
        ),
      ),
    );
  }
}

// Expanded with Stateful widgets //

class ExpandedPage extends StatefulWidget {
  const ExpandedPage({super.key});
  @override
  State<ExpandedPage> createState() => _ExpandedPageState();
}

class _ExpandedPageState extends State<ExpandedPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Flexible(
            fit: FlexFit.loose,
            child: Container(
              padding: const EdgeInsets.all(30),
              color: Colors.orange,
              alignment: Alignment.center,
              height: 150,
              width: 500,
              child: const Text(
                'Ankit',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            )),
        Flexible(
            fit: FlexFit.tight,
            child: Container(
              // decoration:  BoxDecoration(
              //     border: Border(
              //       top: BorderSide(color: Colors.yellow, width: 2),
              //       bottom: BorderSide(color: Color.fromARGB(255, 111, 59, 255), width: 3),
              //       left: BorderSide(color: Color.fromARGB(255, 255, 59, 177), width: 4),
              //       right: BorderSide(color: Color.fromARGB(255, 119, 210, 9), width: 2),
              //       ),
              //       ),
              padding: const EdgeInsets.all(30),
              color: Colors.yellow,
              alignment: Alignment.center,
              height: 150,
              width: 500,
              child: const Text(
                'Abhishek',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            )),
        Expanded(
            flex: 3,
            child: Container(
              padding: const EdgeInsets.all(30),
              color: Colors.blue,
              alignment: Alignment.center,
              height: 150,
              width: 500,
              child: const Text(
                'Sahil',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            )),
        Expanded(
            flex: 4,
            child: Container(
              padding: const EdgeInsets.all(30),
              color: const Color.fromARGB(255, 33, 243, 54),
              alignment: Alignment.center,
              height: 150,
              width: 500,
              child: const Text(
                'Anurag',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            )),
        Flexible(
            fit: FlexFit.tight,
            child: Container(
              padding: const EdgeInsets.all(30),
              color: Colors.pink,
              alignment: Alignment.center,
              height: 50,
              width: 500,
              child: const Text(
                'Amit',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            )),
      ],
    );
  }
}

//Stack Widgets inn stateful //

class StackWidgets extends StatefulWidget {
  const StackWidgets({super.key});
  @override
  State<StackWidgets> createState() => _StackWidgetsState();
}

class _StackWidgetsState extends State<StackWidgets> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Stack(
              alignment: Alignment.center,
              // fit:StackFit.expand,
              // fit: StackFit.loose,
              children: [
            Container(
              height: 350,
              width: 350,
              color: Colors.tealAccent,
            ),
            Container(
              height: 280,
              width: 280,
              color: Colors.limeAccent,
            ),
            Container(
              height: 200,
              width: 200,
              color: Colors.blueAccent,
            ),
            Container(
              height: 150,
              width: 150,
              color: Colors.yellow,
            ),
            Positioned(
              bottom: -250,
              right: -250,
              child: Container(
                height: 110,
                width: 110,
                color: Colors.red,
              ),
            ),
            Container(
              height: 80,
              width: 80,
              color: Colors.white,
            ),
            Container(
              height: 50,
              width: 50,
              color: Colors.pink,
            )
          ])),
    );
  }
}

//
