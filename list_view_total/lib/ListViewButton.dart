import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:list_view_total/pages/All%20widgets.dart';
import 'package:list_view_total/pages/button_navigation.dart';
import 'package:list_view_total/pages/home.dart';
import 'package:list_view_total/pages/login2.dart';
import 'package:list_view_total/pages/loginpage.dart';
import 'package:list_view_total/pages/whatsapp.dart';
import 'package:list_view_total/pages/whatsapp2.dart';
import 'package:list_view_total/profile.dart';
import 'package:list_view_total/tab_layout.dart';

import 'ListView.dart';
import 'ListViewBuilder.dart';
import 'custom_list_view.dart';
import 'list_view_separated.dart';

class ListViewButton extends StatefulWidget {
  const ListViewButton({Key? key}) : super(key: key);

  @override
  State<ListViewButton> createState() => _ListViewButtonState();
}

class _ListViewButtonState extends State<ListViewButton> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Center(child: Text('FLUTTER ALL PROJECT')),
      ),
      body: ButtonBar(alignment: MainAxisAlignment.center, children: [
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'Simple ListView');
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => MyListView()),
                (route) => false);
          },
          child: Text('Simple ListView'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'ListView.builder');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const ListViewBuilder()),
            );
          },
          child: Text('ListView.builder'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'ListView.separated');
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const listviewseparater()),
            );
          },
          child: Text('ListView.seprated'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'TabLayout');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const TabLayout()),
            );
          },
          child: Text('Tablayout+Option'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'CustomListView');
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => const customlistview()),
            );
          },
          child: Text('CustomListView'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'ButtonNavigation');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const ButtonNavigation()),
            );
          },
          child: Text('ButtonNavigation'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'Profile');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Profile()),
            );
          },
          child: Text('Profile'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'Login1');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const Login1()),
            );
          },
          child: Text('Login1'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'WhatsApp');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const WhatsApp()),
            );
          },
          child: Text('Whatsapp'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'WhatsApp2');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const WhatsApp2()),
            );
          },
          child: Text('Whatsapp2'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'LoginPage');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const LoginPage()),
            );
          },
          child: Text('Login2'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'ProfilePage');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => ProfilePage()),
            );
          },
          child: Text('Home'),
        ),
        SizedBox(height: 10),
        ElevatedButton(
          onPressed: () {
            Fluttertoast.showToast(msg: 'All Widgets');
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => AllWidgets()),
            );
          },
          child: Text('AllWidgets'),
        ),
      ]),
      // body: ListView(
      //   children: [
      //     Padding(
      //       padding: const EdgeInsets.only(top: 150, left: 120),
      //       child: Wrap(
      //         children: [
      //           ElevatedButton(
      //               onPressed: () {
      //                 Fluttertoast.showToast(msg: 'Simple ListView');
      //                 Navigator.pushAndRemoveUntil(
      //                     context,
      //                     MaterialPageRoute(builder: (context) => MyListView()),
      //                     (route) => false);
      //               },
      //               child: const Text('Simple ListView')),
      //           const SizedBox(height: 60),
      //           ElevatedButton(
      //               onPressed: () {
      //                 Fluttertoast.showToast(msg: 'ListView.builder');
      //                 Navigator.push(
      //                   context,
      //                   MaterialPageRoute(
      //                       builder: (context) => const ListViewBuilder()),
      //                 );
      //               },
      //               child: const Text('ListView.builder')),
      //           const SizedBox(height: 60),
      //           ElevatedButton(
      //               onPressed: () {
      //                 Fluttertoast.showToast(msg: 'ListView.separated');
      //                 Navigator.push(
      //                   context,
      //                   MaterialPageRoute(
      //                       builder: (context) => const listviewseparater()),
      //                 );
      //               },
      //               child: const Text('ListView.separated')),
      //           const SizedBox(height: 60),
      //           ElevatedButton(
      //               onPressed: () {
      //                 Fluttertoast.showToast(msg: 'CustomListView');
      //                 Navigator.push(
      //                   context,
      //                   MaterialPageRoute(
      //                       builder: (context) => const customlistview()),
      //                 );
      //               },
      //               child: const Text('CustomListView')),
      //           const SizedBox(height: 80),
      //           Padding(
      //               padding: EdgeInsets.only(right: 60),
      //               child: Wrap(
      //                 children: [
      //                   ElevatedButton(
      //                       onPressed: () {
      //                         Fluttertoast.showToast(msg: 'TabLayout');
      //                         Navigator.push(
      //                           context,
      //                           MaterialPageRoute(
      //                               builder: (context) => const TabLayout()),
      //                         );
      //                       },
      //                       child: const Text('TabLayout + OptionMenu')),
      //                 ],
      //               )),
      //           const SizedBox(height: 50),
      //           Padding(
      //               padding: EdgeInsets.only(top: 30),
      //               child: Wrap(
      //                 children: [
      //                   ElevatedButton(
      //                       onPressed: () {
      //                         Fluttertoast.showToast(msg: 'Profile');
      //                         Navigator.push(
      //                           context,
      //                           MaterialPageRoute(
      //                               builder: (context) => const Profile()),
      //                         );
      //                       },
      //                       child: const Text('Profile')),
      //                 ],
      //               )),
      //           const SizedBox(height: 80),
      //           Padding(
      //               padding: EdgeInsets.only(left: 160, right: 20),
      //               child: Wrap(
      //                 children: [
      //                   ElevatedButton(
      //                       onPressed: () {
      //                         Fluttertoast.showToast(msg: 'My Login');
      //                         Navigator.push(
      //                           context,
      //                           MaterialPageRoute(
      //                               builder: (context) => const Login2()),
      //                         );
      //                       },
      //                       child: const Text('My Login')),
      //                 ],
      //               )),
      //           Padding(
      //               padding: EdgeInsets.only(top: 80),
      //               child: Wrap(
      //                 children: [
      //                   ElevatedButton(
      //                       onPressed: () {
      //                         Fluttertoast.showToast(msg: 'Whatsapp');
      //                         Navigator.push(
      //                           context,
      //                           MaterialPageRoute(
      //                               builder: (context) => const WhatsApp()),
      //                         );
      //                       },
      //                       child: const Text('Whatsapp')),
      //                 ],
      //               )),
      //           Padding(
      //               padding: EdgeInsets.only(top: 30),
      //               child: Wrap(
      //                 children: [
      //                   ElevatedButton(
      //                       onPressed: () {
      //                         Fluttertoast.showToast(msg: 'Whatsapp2');
      //                         Navigator.push(
      //                           context,
      //                           MaterialPageRoute(
      //                               builder: (context) => const WhatsApp2()),
      //                         );
      //                       },
      //                       child: const Text('Whatsapp2')),
      //                 ],
      //               )),
      //         ],
      //       ),
      //     ),
      //   ],
      // ),
    );
  }
}
