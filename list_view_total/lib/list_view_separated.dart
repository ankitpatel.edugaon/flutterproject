import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class listviewseparater extends StatefulWidget {
  const listviewseparater({Key? key}) : super(key: key);

  @override
  State<listviewseparater> createState() => _listviewseparaterState();
}

class _listviewseparaterState extends State<listviewseparater> {


  final List<String> entries = <String>[
    'Ankit Patel',
    'Abhishek Kumar',
    'Sahil',
    'Anurag',
    'Nitesh',
    'Sonu',
    'Amit',
    'Golden',
    'Arun',
    'Suraj',
    'Nitish'
  ];
  final List<int> colorCodes = <int>[
    900,
    800,
    700,
    600,
    500,
    400,
    300,
    200,
    100,
    50,
    450
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      padding: const EdgeInsets.all(8),
      itemBuilder: (BuildContext context, index) {
        return Container(
          height: 150,
          color: Colors.amber[colorCodes[index]],
          child: Center(child: Text(entries[index])),
        );
      },
      separatorBuilder: (BuildContext context, index) => const Divider(),
      itemCount: entries.length,
    );
  }
}


