import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyListView extends StatefulWidget {
  const MyListView({Key? key}) : super(key: key);

  @override
  State<MyListView> createState() => _MyListViewState();
}

class _MyListViewState extends State<MyListView> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: const Center(child: Text('Flutter List View',style: TextStyle(fontSize: 25),)),
        ),
        body: ListView(
            children: const [
          ListTile(
              leading: Icon(Icons.access_alarm),
              title: Text("Alarm"),
          ),
              ListTile(
                leading: Icon(Icons.map,size:45,color: Colors.cyan,),
                title: Text('Map'),
              ),
              ListTile(
                leading: Icon(Icons.clear),
                title: Text('Clear'),
              ),
              ListTile(
                leading: Icon(Icons.local_activity),
                title: Text('Local_activity'),
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title:  Text('Logout'),
              ),
              ListTile(
                leading: Icon(Icons.navigation),
                title:  Text('Navigation'),
              ),
              ListTile(
                leading: Icon(Icons.man),
                title: Text("Man"),
              ),
              ListTile(
                leading: Icon(Icons.mail),
                title: Text('Email'),
              ),
              ListTile(
                leading: Icon(Icons.clear),
                title: Text('Clear'),
              ),
              ListTile(
                leading: Icon(Icons.local_activity),
                title: Text('Local_activity'),
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title:  Text('Logout'),
              ),
              ListTile(
                leading: Icon(Icons.navigation),
                title:  Text('Navigation'),
              ),
              ListTile(
                leading: Icon(Icons.access_alarm),
                title: Text("Alarm"),
              ),
              ListTile(
                leading: Icon(Icons.map),
                title: Text('Map'),
              ),
              ListTile(
                leading: Icon(Icons.clear),
                title: Text('Clear'),
              ),
              ListTile(
                leading: Icon(Icons.local_activity),
                title: Text('Local_activity'),
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title:  Text('Logout'),
              ),
              ListTile(
                leading: Icon(Icons.navigation),
                title:  Text('Navigation'),
              ),
              ListTile(
                leading: Icon(Icons.access_alarm),
                title: Text("Alarm"),
              ),
              ListTile(
                leading: Icon(Icons.map),
                title: Text('Map'),
              ),
              ListTile(
                leading: Icon(Icons.clear),
                title: Text('Clear'),
              ),
              ListTile(
                leading: Icon(Icons.local_activity),
                title: Text('Local_activity'),
              ),
              ListTile(
                leading: Icon(Icons.logout),
                title:  Text('Logout'),
              ),
              ListTile(
                leading: Icon(Icons.navigation),
                title:  Text('Navigation'),
              ),
        ]),
      ),
    );
  }
}

// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
//
// class MyListView extends StatefulWidget {
//   const MyListView({Key? key}) : super(key: key);
//
//   @override
//   State<MyListView> createState() => _MyListViewState();
// }
//
// class _MyListViewState extends State<MyListView> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Center(
//           child: Text(
//             "Flutter SimpleList View",
//             style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
//           ),
//         ),
//       ),
//       body:ListView(
//         children: [
//           Container(child: Text("ANKIT PATEL",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("ABHISHEK KUMAR",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("ANURAG KUMAR",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("AMIT SINGH",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("ARUN KUMAR",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("SURAJ SINGH",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("SAHIL KUMAR",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("SONU KUMAR",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("GOLDEN KUMAR",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("NITESH KUMAR",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//           Container(child: Text("NITISH KUMAR YADAV",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Colors.pink)),),
//         ],
//       ),
//     );
//   }
// }
