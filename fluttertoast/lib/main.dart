import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: GridView.count(
            crossAxisCount: 3,
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            children: [
              Container(
                decoration: BoxDecoration(
                    image: DecorationImage(
                        image: NetworkImage(
                            "https://i.pinimg.com/474x/5e/23/1d/5e231daac672818cf5e70bd989c6525b.jpg"),
                        fit: BoxFit.cover)),
              ),
              Container(
                  color: Colors.pinkAccent,
                  child: Center(
                      child: Text(
                    'Ankit',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 25,
                        fontWeight: FontWeight.bold),
                  ),
                  ),
              ),
              Container(
                color: Colors.yellow,
                child: Center(
                  child: Text("Nitesh Raj",
                    style: TextStyle(color: Colors.white,fontSize: 20,fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Container(
                color: Colors.teal,
              ),
              Container(
                color: Colors.indigo,
              ),
              Container(
                color: Colors.blue,
              ),
              Container(
                color: Colors.brown,
              ),
              Container(
                color: Colors.purple,
              ),
              Container(
                color: Colors.pink,
              ),
              Container(
                color: Colors.indigo,
              ),
              Container(
                color: Colors.lightGreen,
              ),
              Container(
                color: Colors.pinkAccent,
              ),
              Container(
                color: Colors.yellow,
              ),
              Container(
                color: Colors.teal,
              ),
              Container(
                color: Colors.indigo,
              ),
              Container(
                color: Colors.blue,
              ),
              Container(
                color: Colors.brown,
              ),
              Container(
                color: Colors.purple,
              ),
              Container(
                color: Colors.pink,
              ),
              Container(
                color: Colors.indigo,
              ),
              Container(
                color: Colors.lightGreen,
              ),
            ]),
      ),
    );
  }
}
